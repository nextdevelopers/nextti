<!DOCTYPE html>
<html lang="pt-BR" dir="ltr">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Agradecimento | Next Tecnologia da Informação</title>
	<meta name="keywords" content="provimento 74, provimento 74 pdf, provimento cnj, soluções para cartórios, provimento 74 ti, soluções provimento 74, provimento 74 cnj, provimto 74, empresa de ti que atende o provimento 74, empresa de ti provimento 74">
	<link rel="short icon" href="favicon.png">
	<link rel="manifest" href="/manifest.json">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link href="assets/lib/animate.css/animate.min.css" rel="stylesheet">
	<link href="assets/lib/simple-text-rotator/simpletextrotator.css" rel="stylesheet">
	<link href="assets/css/agradecimento.min.css" rel="stylesheet">
	<link id="mapa" href="assets/css/mapa.min.css" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Roboto:400,300,500,700,900" rel="stylesheet" type="text/css" />
	<link href="assets/lib/et-line-font/et-line-font.css" rel="stylesheet">
</head>
<body>
<header>
        <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-light justify-content-center">
            <a class="navbar-brand text-center logo-agradecimento" href="http://nexttecnologiadainformacao.com.br/"><img src="assets\images\logo_next.png" alt="Logo oficial da empresa NEXT Tecnologia da Informação" class="img-fluid"></a>
        </nav>
    </header>
    <main class="finalizando">
        <div class="container">
            <div class="row text-center">
                <div class="col-sm-12">
                    <i class="far fa-check-square fa-2x" id="simbolCheck"></i>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-sm-12">
                    <h2>Tudo certo, recebemos o seu cadastro!</h2>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-sm-12">
                    <h3>Nossos consultores irão entrar em contato em breve.<br> Caso queira contato imediato estamos disponibilizando o link para que você possa tirar suas dúvidas agora mesmo via WhatsApp.</h3>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-sm-12">
                    <i class="fab fa-whatsapp fa-4x" id="whatsappIcon"></i>
                </div>
            </div>
            <div class="row text-center" id="celular">
                <div class="col-sm-12">
                    <a id="cliqueWhatsapp" class="btn" href="https://api.whatsapp.com/send?phone=5512991266447&text=Ol%C3%A1!%20Gostaria%20de%20saber%20mais%20detalhes%20sobre%20a%20Next,%20minhas%20d&#250vidas%20s&#227o%20essas:">Clique aqui</a>
                </div>
            </div>
            <div class="row text-center" id="desktop">
                <div class="col-sm-12">
                    <a id="cliqueWhatsapp" class="btn" href="https://web.whatsapp.com/send?phone=5512991266447&text=Ol%C3%A1!%20Gostaria%20de%20saber%20mais%20detalhes%20sobre%20a%20Next,%20minhas%20d&#250vidas%20s&#227o%20essas:">Clique aqui</a>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-sm-12">
                    <a href="http://nexttecnologiadainformacao.com.br/" class="btn btn-default" id="btnVoltar">Voltar</a>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-sm-12">
                    <h4>(12) 99126-6447</h4>
                </div>
            </div>
        </div>
    </main>
    <footer id="footer-agradece">
        <p>2019 - Next Tecnologia da Informação</p>
    </footer>
    <!-- FONTES -->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Volkhov:400i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <!-- FIM/FONTES -->

    <!-- JAVASCRIPTS -->
    <script src="assets/lib/jquery/dist/jquery.min.js"></script>
    <script async defer src="assets/lib/bootstrap/dist/js/bootstrap.min.js"></script>
    <script async defer src="assets/lib/wow/dist/wow.min.js"></script>
    <script async defer src="assets/lib/isotope/dist/isotope.pkgd.min.js"></script>
    <script async defer src="assets/lib/imagesloaded/imagesloaded.pkgd.min.js"></script>
    <script async defer src="assets/lib/flexslider/jquery.flexslider.min.js"></script>
    <script async defer src="assets/lib/owl.carousel/dist/owl.carousel.min.js"></script>
    <script async defer src="assets/lib/smoothscroll.min.js"></script>
    <script async defer src="assets/lib/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
    <script async defer src="assets/lib/simple-text-rotator/jquery.simple-text-rotator.min.js"></script>
    <script async defer src="assets/js/plugins.min.js"></script>
    <script async defer src="assets/js/main.min.js"></script>
    <script src="assets/lib/ouibounce-master/build/ouibounce.min.js"></script>
    
</body>
</html>