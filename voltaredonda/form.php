<?php
    header('Content-Type: text/html; charset=utf-8');
    require 'assets/mailer/PHPMailerAutoload.php';

    $nome = $_POST['nome'];
    $email = $_POST['email'];
    $telefone = $_POST['telefone'];
    $cidade = $_POST['cidade'];
    $empresa = $_POST['empresa'];
    $setor = $_POST['setores'];
    $solicitacao= $_POST['tipoSolicitacao'];
    $msg = $_POST['msg'];

    $salto = "<br/>";    
    date_default_timezone_set('America/Sao_Paulo');
    $data = date("d/m/y"); //pega a data
    $hora = date("H:i"); //pega a hora
    $localizacao = 'Identificação do formulário: Ouvidoria '.$salto.'Local de envio: OUVIDORIA'.$salto;
    $premsg = ('Este é um formulário enviado apartir do site: nexttecnologiadainformacao.com.br'.$salto.'Enviado ás: '.$hora.' do dia '.$data.''.$salto);
    $corpo2 = "$localizacao $salto $premsg $salto Enviado por:\n $nome $salto Telefone:\n $telefone $salto Email:\n $email $salto Cidade:\n $cidade $salto Empresa:\n $empresa  $salto Setor:\n $setor $salto Tipo de Solicitação:\n $solicitacao $salto Assunto:\n $msg";

    if($nome == null || $email == null || $telefone == null || $cidade == null || $setor == null || $solicitacao == null || $msg == null){
        echo "<script>alert('Preencha todos os campos corretamente.');history.back();</script>";
        exit;
    }

    try
    {
        $mail = new PHPMailer();
        $mail-> SetLanguage("pt-br");
        $mail-> IsSMTP();
        $mail-> IsHTML(true);
        $mail-> CharSet = 'UTF-8';
        $mail-> SMTPSecure = 'tls';
        $mail-> SMTPAuth = (true);
        $mail-> Port = 25;
        $mail-> Host = 'mail.nexttecnologiadainformacao.com.br';
        $mail-> Username = "contato@nexttecnologiadainformacao.com.br";
        $mail-> Password = "msxp0810@!"; //recuperar-senha da conta de email
        $mail-> SetFrom("contato@nexttecnologiadainformacao.com.br", 'Contate-nos');//Enviado por...

        $mail-> AddAddress ("contato@nexttecnologiadainformacao.com.br");//Enviar para...
        $mail-> Subject = ('Formulário Next TI - Ouvidoria');
        $mail-> MsgHTML ($corpo2);

        if($mail->send())
        {
            /*echo "<script>alert('E-mail enviado com sucesso. Em breve nossa equipe entrará em contato com você!')</script>";*/
            echo "<script>window.location = 'index';</script>";
            exit;
        }
        else
        {
            echo "<script>alert('Erro ao enviar o e-mail [error 0x0001].')</script>";
            echo "<script>window.location = 'index';</script>";
            exit;
        }
    }
    catch(Exception $e)
    {
        echo "<script>alert('Erro ao enviar o e-mail [error 0x0002].')</script>";
        echo "<script>window.location = 'index';</script>";
        exit;
    }

?>