[
    {
        "id": "1",
        "existe": "false",
        "estado": "Acre",
        "estadoSigla": "BR-AC",
        "cidade": [""]
    },
    {
        "id": "2",
        "existe": "false",
        "estado": "Alagoas",
        "estadoSigla": "BR-AL",
        "cidade": [""]
    },
    {
        "id": "3",
        "existe": "false",
        "estado": "Amapá",
        "estadoSigla": "BR-AP",
        "cidade": [""]
    },
    {
        "id": "4",
        "existe": "true",
        "estado": "Amazonas",
        "estadoSigla": "BR-AM",
        "cidade": ["<a href='https://nexttecnologiadainformacao.com.br/manaus'>Unidade: Manaus</a><br/>"]
    },
    {
        "id": "5",
        "existe": "true",
        "estado": "Bahia",
        "estadoSigla": "BR-BA",
        "cidade": ["<a href='https://nexttecnologiadainformacao.com.br/salvador'>Unidade: Salvador</a><br/><a href='https://nexttecnologiadainformacao.com.br/vitoriadaconquista'>Unidade: Vitória da Conquista</a><br/>"]
    },
    {
        "id": "6",
        "existe": "false",
        "estado": "Ceará",
        "estadoSigla": "BR-CE",
        "cidade": [""]
    },
    {
        "id": "7",
        "existe": "true",
        "estado": "Distrito-Federal",
        "estadoSigla": "BR-DF",
        "cidade": ["<a href='https://nexttecnologiadainformacao.com.br/brasilia'>Unidade: Brasília</a>"]
    },
    {
        "id": "8",
        "existe": "true",
        "estado": "Espírito-Santo",
        "estadoSigla": "BR-ES",
        "cidade": ["<a href='https://nexttecnologiadainformacao.com.br/serra'>Unidade: Serra</a><br><a href='https://nexttecnologiadainformacao.com.br/vilavelha'>Unidade: Vila Velha</a>"]
    },
    {
        "id": "9",
        "existe": "true",
        "estado": "Goiás",
        "estadoSigla": "BR-GO",
        "cidade": ["<a href='https://nexttecnologiadainformacao.com.br/goiania'>Unidade: Goiania</a>"]
    },
    {
        "id": "10",
        "existe": "false",
        "estado": "Maranhão",
        "estadoSigla": "BR-MA",
        "cidade": [""]
    },
    {
        "id": "11",
        "existe": "true",
        "estado": "Mato-Grosso",
        "estadoSigla": "BR-MT",
        "cidade": ["<a href='https://nexttecnologiadainformacao.com.br/cuiaba'>Unidade: Cuiabá</a>"]
    },
    {
        "id": "12",
        "existe": "false",
        "estado": "Mato-Grosso-do-Sul",
        "estadoSigla": "BR-MS",
        "cidade": [""]
    },
    {
        "id": "13",
        "existe": "true",
        "estado": "Minas-Gerais",
        "estadoSigla": "BR-MG",
        "cidade": ["<a href='https://nexttecnologiadainformacao.com.br/formiga'>Unidade: Formiga</a><br/><a href='https://nexttecnologiadainformacao.com.br/ouropreto'>Unidade: Ouro Preto</a>"]
    },
    {
        "id": "14",
        "existe": "true",
        "estado": "Pará",
        "estadoSigla": "BR-PA",
        "cidade": ["<a href='https://nexttecnologiadainformacao.com.br/belem'>Unidade: Belém</a>"]
    },
    {
        "id": "15",
        "existe": "false",
        "estado": "Paraíba",
        "estadoSigla": "BR-PB",
        "cidade": [""]
    },
    {
        "id": "16",
        "existe": "true",
        "estado": "Paraná",
        "estadoSigla": "BR-PR",
        "cidade": ["<a href='https://nexttecnologiadainformacao.com.br/curitiba'>Unidade: Curitiba</a><br><a href='https://nexttecnologiadainformacao.com.br/foz'>Unidade: Foz do Iguaçu</a><br/>"]
    },
    {
        "id": "17",
        "existe": "false",
        "estado": "Pernambuco",
        "estadoSigla": "BR-PE",
        "cidade": [""]
    },
    {
        "id": "18",
        "existe": "false",
        "estado": "Piauí",
        "estadoSigla": "BR-PI",
        "cidade": [""]
    },
    {
        "id": "19",
        "existe": "true",
        "estado": "Rio-de-Janeiro",
        "estadoSigla": "BR-RJ",
        "cidade": ["<a href='https://nexttecnologiadainformacao.com.br/niteroi'>Unidade: Niterói</a><br/><a href='https://nexttecnologiadainformacao.com.br/riodejaneiro'>Unidade: Rio de Janeiro</a><br/><a href='https://nexttecnologiadainformacao.com.br/voltaredonda'>Unidade: Volta Redonda</a>"]
    },
    {
        "id": "20",
        "existe": "false",
        "estado": "Rio-Grande-do-Norte",
        "estadoSigla": "BR-RN",
        "cidade": [""]
    },
    {
        "id": "21",
        "existe": "false",
        "estado": "Rio-Grande-do-Sul",
        "estadoSigla": "BR-RS",
        "cidade": [""]
    },
    {
        "id": "22",
        "existe": "false",
        "estado": "Rondônia",
        "estadoSigla": "BR-RO",
        "cidade": [""]
    },
    {
        "id": "23",
        "existe": "false",
        "estado": "Roraima",
        "estadoSigla": "BR-RR",
        "cidade": [""]
    },
    {
        "id": "24",
        "existe": "false",
        "estado": "Santa-Catarina",
        "estadoSigla": "BR-SC",
        "cidade": [""]
    },
    {
        "id": "25",
        "existe": "true",
        "estado": "São Paulo",
        "estadoSigla": "BR-SP",
        "cidade": ["<a href='https://nexttecnologiadainformacao.com.br/alphaville'>Unidade: Alphaville</a><br/><a href='https://nexttecnologiadainformacao.com.br'>Unidade: Cruzeiro [MATRIZ]</a><br/><a href='https://nexttecnologiadainformacao.com.br/ferrazdevasconcelos'>Unidade: Ferraz de Vasconcelos</a><br/><a href='https://nexttecnologiadainformacao.com.br/mooca'>Unidade: Mooca</a><br/><a href='https://nexttecnologiadainformacao.com.br/santoandre'>Unidade: Santo André</a><br/><a href='https://nexttecnologiadainformacao.com.br/saocarlos'>Unidade: São Carlos</a>"]
    },
    {
        "id": "26",
        "existe": "false",
        "estado": "Sergipe",
        "estadoSigla": "BR-SE",
        "cidade": [""]
    },
    {
        "id": "27",
        "existe": "false",
        "estado": "Tocantins",
        "estadoSigla": "BR-TO",
        "cidade": [""]
    }
]