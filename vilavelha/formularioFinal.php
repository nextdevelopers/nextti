<?php
    header('Content-Type: text/html; charset=utf-8');
    require 'assets/mailer/PHPMailerAutoload.php';

    $nome = $_POST['nome'];
    $email = $_POST['email'];
    $telefone = $_POST['telefone'];
    $cidade = $_POST['cidade'];
    $empresa = $_POST['empresa'];
    $cargo = $_POST['cargo'];
    $quantidadePC = $_POST['qtd'];

    $salto = "<br/>";    
    date_default_timezone_set('America/Sao_Paulo');
    $data = date("d/m/y"); //pega a data
    $hora = date("H:i"); //pega a hora
    //$localizacao = 'Identificação do formulário: Fale Conosco '.$salto.'Local de envio: HOME'.$salto;
    $premsg = ('Olá! Você acaba de receber um possível LEAD de seu site!'.$salto.''.$salto.'Basta agora entrar em contato com ele para verificar a sua necessidade e atendê-lo da melhor maneira possível, seguindo sempre o padrão de qualidade da Next.'.$salto.''.$salto.'Este formulário foi enviado apartir do site: <b>www.nexttecnologiadainformacao.com.br/vilavelha</b>'.$salto.'Enviado ás: '.$hora.' do dia '.$data.''.$salto);
    $corpo2 = "$premsg $salto Enviado por:\n <b>$nome</b> $salto Telefone:\n <b>$telefone</b> $salto Email:\n <b>$email</b> $salto Empresa:\n <b>$empresa</b> $salto Cargo:\n <b>$cargo</b> $salto Quantidade de PCS:\n <b>$quantidadePC</b>";
    /*echo "<script>alert('Seus dados foram recebidos com sucesso.');</script>";*/
    if($nome == null || $email == null || $telefone == null || $cidade == null || $empresa == null || $cargo == null || $quantidadePC == null){
        echo "<script>alert('Preencha todos os campos corretamente.');history.back();</script>";
        exit;
    }

    try
    {
        $mail = new PHPMailer();
        $mail-> SetLanguage("pt-br");
        $mail-> IsSMTP();
        $mail-> IsHTML(true);
        $mail-> CharSet = 'UTF-8';
        $mail-> SMTPSecure = 'tls';
        $mail-> SMTPAuth = (true);
        $mail-> Port = 25;
        $mail-> Host = 'mail.nexttecnologiadainformacao.com.br';
        $mail-> Username = "formulario.vilavelha@nexttecnologiadainformacao.com.br";
        $mail-> Password = "novasenha@2018!"; //recuperar-senha da conta de email
        $mail-> SetFrom("formulario.vilavelha@nexttecnologiadainformacao.com.br", 'Contate-nos');//Enviado por...
        $mail-> AddAddress ("formulario.vilavelha@nexttecnologiadainformacao.com.br");//Enviar para...
        $mail-> Subject = ('Formulário NEXT TI - Fale Conosco');
        $mail-> MsgHTML ($corpo2);

        if($mail->send())
        {
            echo "<script>alert('E-mail enviado com sucesso. Em breve nossa equipe entrará em contato com você!')</script>";
            echo "<script>window.location = 'https://nexttecnologiadainformacao.com.br/vilavelha';</script>";
            exit;
        }
        else
        {
            echo "<script>alert('Erro ao enviar o e-mail [error 0x0001].')</script>";
            echo "<script>window.location = 'https://nexttecnologiadainformacao.com.br/vilavelha';</script>";
            exit;
        }
    }
    catch(Exception $e)
    {
        echo "<script>alert('Erro ao enviar o e-mail [error 0x0002].')</script>";
        echo "<script>window.location = 'https://nexttecnologiadainformacao.com.br/vilavelha';</script>";
        exit;
    }

?>